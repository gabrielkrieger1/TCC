package tcc;

import BEAN.BeanHistorico;
import BEAN.BeanQuestao;
import BEAN.BeanUsuario;
import DAO.DAOHistorico;
import DAO.DAOQuestao;
import DAO.DAOUsuario;
import java.io.IOException;
import java.sql.SQLException;

public class TCC extends PaginasMetodos{
    
    public static void main(String[] args) throws ClassNotFoundException, SQLException, IOException {
        
        PaginasMetodos Pm = new PaginasMetodos();
        
        DAOUsuario DaoUsu = new DAOUsuario();
        DAOQuestao DaoQuest = new DAOQuestao();
        DAOHistorico DaoHist = new DAOHistorico();
        
        BeanUsuario BeanUsu1 = new BeanUsuario ("teste1","11111");
        BeanUsuario BeanUsu2 = new BeanUsuario ("teste2","2222211111");
        BeanUsuario BeanUsu3 = new BeanUsuario ("teste3","333331111111111");
        BeanUsuario BeanUsu4 = new BeanUsuario ("teste4","44444111111111111111");
        BeanUsuario BeanUsu5 = new BeanUsuario ("teste5","5555511111111111111111111");
        BeanQuestao BeanQuest1 = new BeanQuestao ("111111111111111111111","111111111", "Humanas", "Facil");
        BeanQuestao BeanQuest2 = new BeanQuestao ("222222222222222222222","222222222", "Humanas", "Medio");
        BeanQuestao BeanQuest3 = new BeanQuestao ("333333333333333333333","333333333", "Humanas", "Dificil");
        BeanQuestao BeanQuest4 = new BeanQuestao ("444444444444444444444","444444444", "Exatas", "Facil");
        BeanHistorico BeanHist1 = new BeanHistorico (9.0 , "12/12/2012", 1, 1, 1);
        BeanHistorico BeanHist2 = new BeanHistorico (10.0, "11/11/2011", 1, 1, 2);
        BeanHistorico BeanHist3 = new BeanHistorico (11.0, "10/10/2010", 1, 1, 3);
        BeanHistorico BeanHist4 = new BeanHistorico (12.0, "11/11/2012", 1, 1, 4);
        BeanHistorico BeanHist5 = new BeanHistorico (13.0, "11/12/2011", 2, 2, 5);
        BeanHistorico BeanHist6 = new BeanHistorico (14.0, "12/11/2011", 2, 3, 1);
        BeanHistorico BeanHist7 = new BeanHistorico (15.0, "10/10/2012", 3, 1, 2);
        BeanHistorico BeanHist8 = new BeanHistorico (16.0, "10/12/2010", 3, 2, 3);
        BeanHistorico BeanHist9 = new BeanHistorico (17.0, "12/10/2010", 1, 1, 4);
        BeanHistorico BeanHist10 = new BeanHistorico (18.0, "10/10/2011", 1, 2, 5);
        BeanHistorico BeanHist11 = new BeanHistorico (19.0, "10/11/2010", 1, 3, 1);
        BeanHistorico BeanHist12 = new BeanHistorico (20.0, "11/10/2010", 2, 1, 2);
        BeanHistorico BeanHist13 = new BeanHistorico (21.0, "12/11/2010", 2, 2, 3);
        BeanHistorico BeanHist14 = new BeanHistorico (22.0, "11/10/2012", 2, 3, 4);
        BeanHistorico BeanHist15 = new BeanHistorico (23.0, "10/12/2011", 3, 1, 5);
        BeanHistorico BeanHist16 = new BeanHistorico (24.0, Pm.GerarDataHora(), 3, 2, 5);
        
        System.out.println("\nTestando DaoUsu inserir:*********************");
        DaoUsu.InserirUsu(BeanUsu1);
        DaoUsu.InserirUsu(BeanUsu2);
        DaoUsu.InserirUsu(BeanUsu3);
        DaoUsu.InserirUsu(BeanUsu4);
//        DaoUsu.InserirUsu(BeanUsu5);
        
        System.out.println("\nTestando DaoHist inserir e deletar:*********************");
        DaoHist.InserirHist(BeanHist1);
        DaoHist.InserirHist(BeanHist2);
        DaoHist.InserirHist(BeanHist3);
        DaoHist.InserirHist(BeanHist4);
        DaoHist.InserirHist(BeanHist5);
        DaoHist.InserirHist(BeanHist6);
        DaoHist.InserirHist(BeanHist7);
        DaoHist.InserirHist(BeanHist8);
        DaoHist.InserirHist(BeanHist9);
        DaoHist.InserirHist(BeanHist10);
        DaoHist.InserirHist(BeanHist11);
        DaoHist.InserirHist(BeanHist12);
        DaoHist.InserirHist(BeanHist13);
        DaoHist.InserirHist(BeanHist14);
        DaoHist.InserirHist(BeanHist15);
        DaoHist.InserirHist(BeanHist16);
        DaoHist.InserirHist(BeanHist1);
        DaoHist.InserirHist(BeanHist2);
        DaoHist.InserirHist(BeanHist3);
        DaoHist.InserirHist(BeanHist4);
        DaoHist.InserirHist(BeanHist2);
        DaoHist.InserirHist(BeanHist4);
        DaoHist.InserirHist(BeanHist3);
        //DaoHist.DeletarHist(9,9);// id do historico 9,0
        //DaoHist.DeletarHistConta(2);// id do usuario 10,0
        
        System.out.println("\nTestando DaoUsu deletar e alterar:*********************");
        DaoUsu.DeletarUsu(3);
        DaoUsu.AlterarUsu(1, new BeanUsuario("teste6", "66666"));
        DaoUsu.LogarUsu(BeanUsu4);
        //DaoUsu.AlterarUsu(UsuarioId(BeanUsu1),"lala" ,"lele");
        
        System.out.println("\nTestando DaoQuest inserir e deletar:**************************");
//        DaoQuest.InserirQuest(BeanQuest1);
//        DaoQuest.InserirQuest(BeanQuest2);
//        DaoQuest.InserirQuest(BeanQuest3);
//        DaoQuest.InserirQuest(BeanQuest4);
        DaoQuest.DeletarQuest(1);
        
        Pm.ImprimirUsus();
        Pm.PaginaInicial();
    }
}
