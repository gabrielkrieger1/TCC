package DAO;

import BEAN.BeanHistorico;
//import Graficos.TempoResposta;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DAOHistorico extends BaseDAO {

    public void InserirHist(BeanHistorico hist) throws IOException{

        try (Connection conexao = getConexao()){
            String sql = "insert into Historico (MediaQuiz, DataHora, Area, Dificuldade, Usuario_FK) values (?, ?, ?, ?, ?);";
            PreparedStatement pstmt = conexao.prepareStatement(sql,  PreparedStatement.RETURN_GENERATED_KEYS);
            pstmt.setDouble(1, hist.getMedia());
            pstmt.setString(2, hist.getData());
            pstmt.setInt(3, hist.getArea());
            pstmt.setInt(4, hist.getDificuldade());
            pstmt.setLong(5, hist.getUsuario());
            pstmt.executeUpdate();
            
            ResultSet rs = pstmt.getGeneratedKeys();
            rs.next();
            long id_hist = rs.getLong(1);
            
            //TempoResposta grafico = new TempoResposta(hist.getUsuario(), id_hist);
            //grafico.pack();
            
            pstmt.close();
            conexao.close();
            System.out.println("\nHistórico inserido com sucesso!");
        } catch (SQLException | ClassNotFoundException ex) {
            
            System.out.println("\nErro ao cadastrar histórico!");
            Logger.getLogger(DAOHistorico.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void DeletarHist(long idHist, long idUsu) throws ClassNotFoundException{

        // como faço para pegar o id do usuário pra excluir o gráfico?
        try (Connection conexao = getConexao()){
            String sql = "delete from Historico where Id_historico = ?";
            PreparedStatement pstmt = conexao.prepareStatement(sql);
            pstmt.setLong(1, idHist);
            pstmt.executeUpdate();
                   
            DAOUsuario dao = new DAOUsuario();
            
            if (dao.DeletarGrafico(idUsu + "", idHist + "")){
                System.out.println("\nHistórico excluído com sucesso!");
            } else {
                System.out.println("\nErro ao excluir histórico!");
            }
            //ResultSet rs = pstmt.executeQuery(sql);
            //long id_usuario = rs.getLong("Usuario_FK");
            /*String grafico = "C:\\Users\\Milena Marcos\\Desktop\\TCC Atualizado\\Gráficos (por Id_Usuario)\\" + id_usuario + 
                    " (por Id_Historico)\\" + idHist + ".png"; 
            File f = new File(grafico);  
            f.delete();*/
            
            pstmt.close();
            conexao.close();
            
        } catch (SQLException | ClassNotFoundException ex) {
            
            System.out.println("\nErro ao excluir histórico!");
            Logger.getLogger(DAOUsuario.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public boolean DeletarHistConta(long idUsu) throws ClassNotFoundException{

        try (Connection conexao = getConexao()){
            String sql = "delete from Historico where Usuario_FK = ?";
            PreparedStatement pstmt = conexao.prepareStatement(sql);
            pstmt.setLong(1, idUsu);
            pstmt.executeUpdate();
            pstmt.close();
            conexao.close();
            
            System.out.println("\nTodo o histórico foi excluido com sucesso!");
            return true;
            
        } catch (SQLException | ClassNotFoundException ex) {
            
            System.out.println("\nErro ao excluir todo o histórico!");
            Logger.getLogger(DAOUsuario.class.getName()).log(Level.SEVERE, null, ex);
        }        
        return false;
    }
    
    public ArrayList<BeanHistorico> getCadastrosHist (String comando) throws ClassNotFoundException {
        
        ArrayList<BeanHistorico> Cadastros = new ArrayList();

        try (Connection conn = getConexao()) {
            String sql = "select * from Historico" + comando;
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.executeQuery();
            ResultSet rs = ps.getResultSet();
            while (rs.next()) {
                Cadastros.add(new BeanHistorico(rs.getLong(1), rs.getDouble(2), rs.getString(3), rs.getInt(4), rs.getInt(5), rs.getLong(6)));
            }
            rs.close();
            ps.close();
            
        } catch (SQLException ex) {
            System.out.println(ex);
        }
        return Cadastros;
    }
}
