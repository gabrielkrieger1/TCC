Drop database if exists TCC;
Create database TCC;
Use TCC;

Create table Usuario(
	Id_usuario integer auto_increment,
	Nome varchar(25),
	Senha varchar(50),
	Primary key (id_usuario)
);
Create table Historico(
	Id_historico integer auto_increment,
	MediaQuiz double not null,
	DataHora varchar(50) not null,
        Area varchar(15) not null,
        Dificuldade varchar(15) not null,
        Usuario_FK integer(10) not null,
	Primary key (Id_historico),
        Foreign key (Usuario_FK) references Usuario (Id_usuario)
);
Create table Questao(
	Id_questao integer auto_increment,
	Descricao varchar(100),
	Observacao varchar(100),
        Area int not null,
        Dificuldade int not null,
	Primary key (Id_questao)
);

/*insert into Dificuldade (Desc_dificuldade) values ("Facil");
insert into Dificuldade (Desc_dificuldade) values ("Médio");
insert into Dificuldade (Desc_dificuldade) values ("Difícil");
insert into Area (Desc_area) values ("Humanas");
insert into Area (Desc_area) values ("Exatas");
insert into Area (Desc_area) values ("Biológicas");*/

Create table Dificuldade(
	Id_dificuldade integer auto_increment,
   	Desc_dificuldade varchar(25),
 	Primary key (Id_dificuldade)
);
Create table Area(
 	Id_area integer auto_increment,
	Desc_area varchar(25),
	Primary key (Id_area)
);

--insert into Usuario (Nome, Senha) values ("teste","teste");

--insert into Historico (MediaQuiz, DataHora, Area_FK, Dificuldade_FK, Usuario_FK) values (10.0, "Data e Hora", 1, 1, 1);

--insert into Questao (Descricao, Observacao, Area_FK, Dificuldade_FK) values ("Em que ano ocorreu a crise de 29?","1929", 3, 1);

--select * from Questao

--insert into Usuario (Nome, Senha) values ("teste","1234");

--select * from Historico where Usuario_FK = 1 and DataHora = "12/%";

--select * from Historico where DataHora like "31%";