create schema if not exists TCC;
drop database tcc;
create database tcc;

create table usuario(
   id int(20),
   nome varchar(70),
   senha varchar(20),
   primary key (id)
);

create table questao(
   id int(20),
   descricao varchar(100),
   observacao varchar(100),
   