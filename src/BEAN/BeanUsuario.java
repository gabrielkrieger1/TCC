package BEAN;

public class BeanUsuario {
    
    private long id;
    private String nome;
    private String senha;

    public BeanUsuario(){
    }
    
    public BeanUsuario(long id, String nome, String senha){
        setId(id);
        setNome(nome);
        setSenha(senha);
    }
    
    public BeanUsuario(String nome, String senha){
        setNome(nome);
        setSenha(senha);
    }
    
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}