package BEAN;

import DAO.DAOHistorico;
import DAO.DAOQuestao;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import tcc.PaginasMetodos;

public class Quiz extends PaginasMetodos{
    
    private ArrayList <String> ArrayQuestao;//15
    private Long[] ArrayTempo;//10
    private int Contador = 0;
    private int Area;
    private int Dificuldade;

    public Quiz(ArrayList<String> Questao, Long[] Tempo) {
        setArrayQuestao(Questao);
        setArrayTempo(Tempo);
    }
    
    public Quiz(){
    }
    
    public ArrayList <String> SortearQuests (int area, int dific) throws ClassNotFoundException{
        
        DAOQuestao dao = new DAOQuestao();
        String comando = "";
        ArrayList <String> descricao = null;
     
        if (area != 4 && dific != 4){
            comando = " where Area_FK = " + area + " and Dificuldade_FK = " + dific;
        } else {
            
            if (area == 4 && dific != 4){                
                comando = " where Dificuldade_FK = " + dific;
            }
            if (dific == 4 && area != 4)
                comando = " where Area_FK = " + area;
            }
            
        for (BeanQuestao lista : dao.getCadastrosQuest("Descricao" ,comando + " order by rand() limit 1,15")){
            descricao.add(lista.getDescricao());
        }
        
        return descricao;
    }
    
    public void IniciarQuiz() throws ClassNotFoundException, SQLException, IOException{
        
        setArea (PedirInt("\n1 - Exatas, 2 - Humanas, 3 - Biológicas, 4 - Indefinir\nInforme a área:", 4));
        setDificuldade (PedirInt("\n1 - Fácil, 2 - Médio, 3 - Difícil, 4 - Indefinir\nInforme a dificuldade:", 4));
        
        setArrayQuestao(SortearQuests(getArea(), getDificuldade()));
        
        ContinuarQuiz();
    }
    
    public void ContinuarQuiz () throws SQLException, ClassNotFoundException, IOException{
        
        if (getContador()<=15){
                
            System.out.println(getArrayQuestao().get(getContador()));
            
            setContador(getContador() + 1);
            
            // apenas para preecher o ArrayTempo
            if (getContador()>5){
                ArrayTempo[getContador() - 5] = (long) getContador();
            }

            if (PedirInt("\nInforme 1 para continuar e 2 para desistir", 2) == 1){
                ContinuarQuiz();
            } else {
                DesistirQuiz();
            }
        } else {
            
            DAOHistorico dao = new DAOHistorico();
            dao.InserirHist( new BeanHistorico(CalcularMedia(), GerarDataHora(), getArea(), getDificuldade(), getIdUsu()));
            getMenu();
        }        
    }
    
    public void DesistirQuiz() throws SQLException, ClassNotFoundException, IOException{
        
        if (PedirInt("\nVocê realmente quer desistir do quiz? 1 - sim e 2 - não:", 2) == 1){
            getMenu();
        } else {
            ContinuarQuiz();
        }
    }
    
    public double CalcularMedia(){
        
        double media = 0;
        
        for (int i = 0; i < 15; i++){
            media += ArrayTempo[i];
        }
        
        return media;
    }
    
    public ArrayList <String> getArrayQuestao() {
        return ArrayQuestao;
    }

    public void setArrayQuestao(ArrayList <String> Questao) {
        this.ArrayQuestao = Questao;
    }

    public int getArea() {
        return Area;
    }

    public void setArea(int Area) {
        this.Area = Area;
    }

    public int getDificuldade() {
        return Dificuldade;
    }

    public void setDificuldade(int Dificuldade) {
        this.Dificuldade = Dificuldade;
    }
    
    public int getContador() {
        return Contador;
    }

    public void setContador(int Contador) {
        this.Contador = Contador;
    }

    public Long[] getArrayTempo() {
        return ArrayTempo;
    }

    public void setArrayTempo(Long[] Tempo) {
        this.ArrayTempo = Tempo;
    }
}
