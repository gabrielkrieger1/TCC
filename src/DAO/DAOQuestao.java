package DAO;

import BEAN.BeanQuestao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DAOQuestao extends BaseDAO {
 
    public void InserirQuest (BeanQuestao quest) throws ClassNotFoundException {

        try (Connection conexao = getConexao()){
            String sql = "insert into Questao(Descricao, Observacao, Area, Dificuldade) values (?, ?, ?, ?);";
            PreparedStatement pstmt = conexao.prepareStatement(sql);
            pstmt.setString(1, quest.getDescricao());
            pstmt.setString(2, quest.getObservacao());
            pstmt.setString(3, quest.getArea());
            pstmt.setString(4, quest.getDificuldade());
            pstmt.executeUpdate();
            pstmt.close();
            
            System.out.println("\nQuestão inserida com sucesso!");
            
            pstmt.close();
            conexao.close();
            
        } catch (SQLException ex) {
            
            System.out.println("\nErro ao inserir questão!");
            Logger.getLogger(DAOUsuario.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void DeletarQuest (long id) throws ClassNotFoundException {

        try (Connection conexao = getConexao()){
            String sql = "delete from Questao where Id_Questao = ?";
            PreparedStatement pstmt = conexao.prepareStatement(sql);
            pstmt.setLong(1, id);
            pstmt.executeUpdate();
            pstmt.close();

            System.out.println("\nQuestão excluída com sucesso!");
            
        } catch (SQLException ex) {
            
            System.out.println("\nErro ao excluir questão!");
            Logger.getLogger(DAOUsuario.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void AlterarQuest(long id, BeanQuestao quest) throws SQLException, ClassNotFoundException {
    //public void AlterarQuest(long id, String desc, String observ, int area, int dific) throws SQLException, ClassNotFoundException {
        
        try (Connection conexao = getConexao()){
            
                BeanQuestao questao = new BeanQuestao(id, quest.getDescricao(), quest.getObservacao(), quest.getArea(), quest.getDificuldade());
                PreparedStatement stmt2 = null;
                String sql = "update Questao set Descricao = ?, Observacao = ?, Area = ?, Dificuldade = ? where Id_questao = " + id;
                stmt2 = conexao.prepareStatement(sql);
                stmt2.setString(1, questao.getDescricao());
                stmt2.setString(2, questao.getObservacao());
                stmt2.setString(3, questao.getArea());
                stmt2.setString(4, questao.getDificuldade());
                stmt2.execute();
                stmt2.close();
                
                System.out.println("\nAlteração feita com sucesso!");
                
            } catch (SQLException | ClassNotFoundException e) {
            
            System.out.println("\nErro ao alterar questão!");    
            System.out.println(e);
        }
    }
    
    public ArrayList<BeanQuestao> getCadastrosQuest (String comando1, String comando2) throws ClassNotFoundException {
        
        ArrayList<BeanQuestao> Cadastros = new ArrayList();

        try (Connection conn = getConexao()) {
            String sql = "select " + comando1 + " from Questao" + comando2;
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.executeQuery();
            ResultSet rs = ps.getResultSet();
            while (rs.next()) {
                Cadastros.add(new BeanQuestao(rs.getLong(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5)));
            }
            rs.close();
            ps.close();
            
        } catch (SQLException ex) {
            System.out.println(ex);
        }
        return Cadastros;
    }
    
}
