package DAO;

import BEAN.BeanUsuario;
import java.io.File;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DAOUsuario extends BaseDAO {

    public static boolean ValidarUsu(BeanUsuario usu, Connection conn) throws SQLException, ClassNotFoundException{
   
            String sql = "select * from Usuario";
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.executeQuery();
            ResultSet rs = ps.getResultSet();            
            while (rs.next()){
                if (rs.getString(2).equals(usu.getNome()) || usu.getSenha().length()<5 || usu.getNome().length()<5){
                    return false;
                }
            }
            rs.close();
            ps.close();
            
        return true;
    }
    
    public boolean CriarPastaGraficos(String id_usuario){  
        String NovaPasta = null;   
        try {       
        NovaPasta = "C:\\Users\\Milena Marcos\\Desktop\\TCC Atualizado\\Gráficos (por Id_Usuario)\\" + id_usuario + " (por Id_Historico)";   
            if (!new File(NovaPasta).exists()) { // Verifica se o diretório existe.   
                (new File(NovaPasta)).mkdir();   // Cria o diretório  
                return true;
            }            
        } catch (Exception ex) {   
            System.out.println(ex);
        }
        return false;
    }  
    
    public boolean DeletarGrafico(String id_usuario, String id_grafico){
        
        try{
        String caminho = "C:\\Users\\Milena Marcos\\Desktop\\TCC Atualizado\\Gráficos (por Id_Usuario)\\" + id_usuario + " (por Id_Historico)\\" + id_grafico + ".png";
        File f = new File(caminho); 
        f.delete();
        return true;
        } catch (Exception ex){
            System.out.println("Erro: " + ex);
            return false;
        }
    }
    
    public boolean DeletarPastaGraficos(String id_usuario){
         
        File folder = new File("C:\\Users\\Milena Marcos\\Desktop\\TCC Atualizado\\Gráficos (por Id_Usuario)\\" + id_usuario + " (por Id_Historico)");   
        
        if (folder.list().length>0){
            File[] sun = folder.listFiles();
            for (File toDelete : sun) {  
                toDelete.delete();  
            }
        }
        folder.delete();
        System.out.println("\nPasta exluída com sucesso!");
        return true;
    }
    
    public void InserirUsu(BeanUsuario usu) throws ClassNotFoundException {

        try (Connection conexao = getConexao()){

            if (ValidarUsu(usu, conexao)){
                System.out.println("\n-----------\nNome: " + usu.getNome());
                System.out.println("Senha: " + stringHexa(gerarHash(usu.getSenha())));
                System.out.println("Tamanho da Senha: " + stringHexa(gerarHash(usu.getSenha())).length());
                String sql = "insert into Usuario(Nome, Senha) values (?, ?);";
                PreparedStatement pstmt = conexao.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
                pstmt.setString(1, usu.getNome());
                pstmt.setString(2, stringHexa(gerarHash(usu.getSenha())));
                pstmt.execute();
                
                System.out.println("\nCadastro realizado com sucesso!");
                
                ResultSet rs = pstmt.getGeneratedKeys();
                rs.next();
                long id = rs.getLong(1);
                pstmt.close();
                
                if (CriarPastaGraficos(id+"")){
                    System.out.println("\nPasta de arquivos criada com sucesso!");
                }else{
                    System.out.println("\nErro ao criar a pasta de arquivos!");
                }
            } else {
                System.out.println("\nNome de usuário já existente e/ou senha inferior a 5 caracteres!");
            }

        } catch (SQLException | ClassNotFoundException ex) {

            Logger.getLogger(DAOUsuario.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void DeletarUsu(long id) throws ClassNotFoundException{

        DAOHistorico daoHist = new DAOHistorico();
            if (daoHist.DeletarHistConta(id) && DeletarPastaGraficos(id+"")){        
                try (Connection conexao = getConexao()){
                    String sql = "delete from Usuario where Id_usuario = ?";
                    PreparedStatement pstmt = conexao.prepareStatement(sql);
                    pstmt.setLong(1, id);
                    pstmt.executeUpdate();
                    pstmt.close();
                    System.out.println("\nUsuário excluido com sucesso!");

                } catch (SQLException | ClassNotFoundException ex) {
                    System.out.println("\nErro ao excluir usuário!");
                    Logger.getLogger(DAOUsuario.class.getName()).log(Level.SEVERE, null, ex);
                }
         } else {
                System.out.println("\nErro ao excluir usuário!");
         }
    }
    
    public void AlterarUsu(long id, BeanUsuario bean) throws SQLException, ClassNotFoundException {
        
        try (Connection conexao = getConexao()){
            
            if (ValidarUsu(bean, conexao) == true){
            
                BeanUsuario usu = new BeanUsuario(id, bean.getNome(), stringHexa(gerarHash(bean.getSenha())));
                PreparedStatement stmt2 = null;
                String sql = "update Usuario set Nome = ?, Senha = ? where Id_usuario = " + usu.getId();
                stmt2 = conexao.prepareStatement(sql);
                stmt2.setString(1, usu.getNome());
                stmt2.setString(2, usu.getSenha());
                stmt2.execute();
                stmt2.close();
                
                System.out.println("\nAlteração feita com sucesso!");
            } else {
                
                System.out.println("\nNome de usuário já existente e/ou senha inferior a 5 caracteres!");
            }
        } catch (SQLException | ClassNotFoundException e) {
            
            System.out.println(e);
        }
    }
    
    public long LogarUsu(BeanUsuario usu) throws SQLException, ClassNotFoundException{
        
        try (Connection conn = getConexao()) {
            String sql = "select * from Usuario";
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.executeQuery();
            ResultSet rs = ps.getResultSet();            
            while (rs.next()){
                if (rs.getString(2).equals(usu.getNome()) && rs.getString(3).equals(stringHexa(gerarHash(usu.getSenha())))){
                    return rs.getLong(1);
                }
            }
            rs.close();
            ps.close();
            
        } catch (SQLException ex) {
            System.out.println("\nErro ao logar!");
        }
        return 0;
    }
    
    public static byte[] gerarHash(String senha) {
        try {
          MessageDigest md = MessageDigest.getInstance("MD5");
          md.update(senha.getBytes());
          return md.digest();
        } catch (NoSuchAlgorithmException e) {
            System.out.println(e);
          return null;
        }
    }
    
    private static String stringHexa(byte[] bytes) {
       StringBuilder s = new StringBuilder();
       for (int i = 0; i < bytes.length; i++) {
           int parteAlta = ((bytes[i] >> 4) & 0xf) << 4;
           int parteBaixa = bytes[i] & 0xf;
           if (parteAlta == 0) s.append('0');
           s.append(Integer.toHexString(parteAlta | parteBaixa));
       }
       return s.toString();
    }
    
    public ArrayList<BeanUsuario> getCadastrosUsu() throws ClassNotFoundException {
        
        ArrayList<BeanUsuario> Cadastros = new ArrayList();

        try (Connection conn = getConexao()) {
            String sql = "select * from Usuario";
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.executeQuery();
            ResultSet rs = ps.getResultSet();
            while (rs.next()) {
                Cadastros.add(new BeanUsuario(rs.getLong(1), rs.getString(2), rs.getString(3)));
            }
            rs.close();
            ps.close();
            
        } catch (SQLException ex) {
            System.out.println(ex);
        }
        return Cadastros;
    }
}