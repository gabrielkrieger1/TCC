package BEAN;

public class BeanQuestao{
    
    private long id;
    private String descricao, observacao, area, dificuldade;
    
    public BeanQuestao(){
    }
    
    public BeanQuestao(long id, String descricao, String observacao){
        setId(id);
        setDescricao(descricao);
        setObservacao(observacao);
    }
    
    public BeanQuestao(String descricao, String observacao, String area, String dificuldade){
        setDescricao(descricao);
        setObservacao(observacao);
        setArea(area);
        setDificuldade(dificuldade);
    }
    
    public BeanQuestao(long id, String descricao, String observacao, String area, String dificuldade){
        setId(id);
        setDescricao(descricao);
        setObservacao(observacao);
        setArea(area);
        setDificuldade(dificuldade);
    }
    
    public void setArea(String area) {
        this.area = area;
    }
    
    public String getArea() {
        return area;
    }

    public String getDificuldade() {
        return dificuldade;
    }

    public void setDificuldade(String dificuldade) {
        this.dificuldade = dificuldade;
    }

    public long getId() {
        return id;
    }
    
    public void setId(long id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }
}
