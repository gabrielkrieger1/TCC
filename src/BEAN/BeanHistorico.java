package BEAN;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;

public class BeanHistorico{
    
    private long id;
    private double media;
    private String data;
    private long usuario;
    private int area, dificuldade;

    public BeanHistorico(long id, double media, String data, int area, int dificuldade, long usuario){
        setId(id);
        setData(data);
        setMedia(media);
        setArea(area);
        setDificuldade(dificuldade);
        setUsuario(usuario);
    }
    
    public BeanHistorico(long id, double media, String data, int area, int dificuldade){
        setId(id);
        setData(data);
        setMedia(media);
        setArea(area);
        setDificuldade(dificuldade);
    }
    
    public BeanHistorico(double media, String data, int area, int dificuldade, long usuario){
        setData(data);
        setMedia(media);
        setArea(area);
        setDificuldade(dificuldade);
        setUsuario(usuario);
    }
    
    public BeanHistorico(){
    }

    public int getArea() {
        return area;
    }

    public void setArea(int area) {
        this.area = area;
    }

    public int getDificuldade() {
        return dificuldade;
    }

    public void setDificuldade(int dificuldade) {
        this.dificuldade = dificuldade;
    }

    public long getUsuario() {
        return usuario;
    }

    public void setUsuario(long usuario) {
        this.usuario = usuario;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getMedia() {
        return media;
    }

    public void setMedia(double media) {
        this.media = media;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
    
    public static String GerarDataHora(){
        Calendar c = Calendar.getInstance();
        Date data = c.getTime();
        DateFormat dtHora = DateFormat.getDateTimeInstance();
        
        return dtHora.format(data);
    }
}