package DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class BaseDAO {

    final String DRIVERNAME = "com.mysql.jdbc.Driver";
    final String DBURL = "jdbc:mysql://localhost:3306/tcc";
    final String USUARIO = "root";
    final String SENHA = "root";

    public Connection getConexao() throws SQLException, ClassNotFoundException {

        Connection conexao;
        Class.forName(DRIVERNAME);
        conexao = DriverManager.getConnection(DBURL, USUARIO, SENHA);
        return conexao;
    }
}
