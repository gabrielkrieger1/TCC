package tcc;

import BEAN.BeanHistorico;
import BEAN.BeanQuestao;
import BEAN.BeanUsuario;
import BEAN.Quiz;
import DAO.DAOHistorico;
import DAO.DAOQuestao;
import DAO.DAOUsuario;
import java.io.IOException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

public class PaginasMetodos {
    
    private long idUsu;

    public long getIdUsu() {
        return idUsu;
    }

    public void setIdUsu(long id) {
        this.idUsu = id;
    }
    
    public int PedirInt(String mensag, int max){
         
        Scanner scan = new Scanner(System.in);
        
        System.out.println(mensag);
        int resp = scan.nextInt();
        
        if (resp < 1 || resp > max){
            PedirInt(mensag, max);
        }
        return resp;
    }
    
    public String PedirString(String mensag){
        
        Scanner scan = new Scanner(System.in);
        
        System.out.println(mensag);
        String resp = scan.nextLine();
        
        if (resp.equals("")){
            PedirString(mensag);
        }   
        return resp;
    }
    
    public BeanQuestao PedirQuest (String texto){
    
        return new BeanQuestao(    
        PedirString("\nInforme a " + texto + "descrição:"),
        PedirString("\nInforme uma " + texto + "observação:"),
        PedirString("\nHumanas, Exatas, Biológicas\nInforme a " + texto + "área:"),
        PedirString("\nFácil, Médio, Difícil\nInforme a " + texto + "dificuldade:"));
    }
    
    public BeanUsuario PedirUsu (String texto){
        
        return new BeanUsuario(PedirString("\nInforme o " + texto + "nome:"),PedirString("\nInforme a " + texto + "senha"
                + " (mínimo 5 caracteres):"));
    }
    
    public String GerarDataHora(){
        Calendar c = Calendar.getInstance();
        Date data = c.getTime();
        DateFormat dtHora = DateFormat.getDateTimeInstance();
        
        return dtHora.format(data);
    }
    
    public long ImprimirQuests (String comando) throws ClassNotFoundException{
        DAOQuestao array = new DAOQuestao();
        long id = 0;
        System.out.println("\n|Id:   |Descrição:           |Observação:            |Área:   |Dificuldade:  |");
        for (BeanQuestao q : array.getCadastrosQuest("*", comando)){
            System.out.println("|" + q.getId() + "     |" + q.getDescricao() + "|" + q.getObservacao() + "               |" + q.getArea() + "       |" + q.getDificuldade() + "             |");
            if (q.getId()>id){
                id = q.getId();
            }
        }
        return id;
    }
    
    public long ImprimirHists (String comando, String mensagem) throws ClassNotFoundException{
        DAOHistorico array = new DAOHistorico();
        long id = 0;
        System.out.println(mensagem + "\n|Id:   |Média:           |Data/Hora:           |Área:           |Dificuldade:          |Id do Usuario:       ");
        for (BeanHistorico q : array.getCadastrosHist(comando)){
            System.out.println("|" + q.getId() + "     |" + q.getMedia() + "         |" + q.getData() + "          |" + q.getArea() + "      |" + 
                    q.getDificuldade() + "       |" + q.getUsuario() + "        |");
            if (q.getId()>id){
                id = q.getId();
            }
        }
        return id;
    }
    
    public void ImprimirUsus() throws ClassNotFoundException{
        DAOUsuario array = new DAOUsuario();
        System.out.println("\n|Id:   |Nome:           |Senha:           |");
        for (BeanUsuario q : array.getCadastrosUsu()){
            System.out.println("|" + q.getId() + "     |" + q.getNome() + "          |" + q.getSenha() + "            |");
        }        
    }
    
    public void PaginaInicial() throws SQLException, ClassNotFoundException, IOException{
        
        DAOUsuario dao = new DAOUsuario();
        
        if (PedirInt("\nInforme 1 para cadastrar novo usuário e 2 para logar usuário:", 2) == 1){
            dao.InserirUsu(PedirUsu(""));
        }else{
            setIdUsu(dao.LogarUsu(PedirUsu("")));
            if (getIdUsu() != 0){
                getMenu();
            }
        }
        System.out.println("\nErro ao logar!");
        PaginaInicial();
    }
    
    public void getMenu() throws SQLException, ClassNotFoundException, IOException{
        
        int resp = PedirInt("\n1 - Perfil\n2 - Quiz (em manutenção)\n3 - Questões\n4 - Histórico\n5 - Listar todos os "
                + "Usuários\n6 - Listar todos os Históricos\n7 - Listar todas as Questões\n8 - Voltar\n9 - Sair\nDigite aonde você "
                + "quer ir:", 9);
        
        switch (resp){
            case 1:
                PerfilUsu();
            break;
            case 2:
                // Quiz: Iniciar, Continuar e Desistir
                QuizUsu();
            break;
            case 3:
                Questoes();
            break;
            case 4:
                Historicos();
            break;
            case 5:
                ImprimirUsus();
            break;
            case 6:
                ImprimirHists("", "");
            break;
            case 7:
                ImprimirQuests("");
            break;
            case 8:
                PaginaInicial();
            break;
            default:
                System.exit(1);
            break;
        }
        getMenu();
    }
    
    public void QuizUsu() throws ClassNotFoundException, SQLException, IOException{
        
        Quiz quiz = new Quiz();
        quiz.IniciarQuiz();
        
    }
    
    public void PerfilUsu() throws SQLException, ClassNotFoundException, IOException{
        
        DAOUsuario dao = new DAOUsuario();
        
        if (PedirInt("\nInforme 1 para alterar nome/senha e 2 para desativar conta:", 2) == 1){
            dao.AlterarUsu(idUsu, PedirUsu("novo(a) "));
        }else{
            dao.DeletarUsu(idUsu);
            PaginaInicial();
        }
        getMenu();
    }
    
    public void Questoes() throws ClassNotFoundException, SQLException, IOException{
        
        int visualizacao = PedirInt("\n1 - Por área e dificuldade\n2 - Somente por área\n3 - Somente por dificuldade"
            + "\n4 - Tudo\nInforme o tipo de visualização:", 4), resp = 0;
        String comando = " where";
        
        switch (visualizacao){
            case 1:
                resp = PedirInt("\n1 - Exatas\n2 - Humanas\n3 - Biológicas\nInforme a área:", 3);
                int resp2 = PedirInt("\n1 - Fácil\n2 - Médio\n3 - Difícil\nInforme a dificuldade:", 3);
                comando += " Area = " + resp + " and Dificuldade = " + resp2;
                break;
                
            case 2:
                resp = PedirInt("\n1 - Exatas\n2 - Humanas\n3 - Biológicas\nInforme a área:", 3);
                comando += " Area = " + resp;
                break;
                
            case 3:
                resp = PedirInt("\n1 - Fácil\n2 - Médio\n3 - Difíci\nInforme a dificuldade:", 3);
                comando += " Dificuldade = " + resp;
                break;
            default:
                comando = "";
        }
        
        long idMax = ImprimirQuests(comando);
        
        DAOQuestao dao = new DAOQuestao();
        int resp3 = PedirInt("\nInforme 1 para cadastrar, 2 para alterar, 3 para excluir questão e 4 para voltar:", 4);
        
        if (resp3 == 1){
            dao.InserirQuest(PedirQuest(""));
        } 
        if (resp3 == 2){
            dao.AlterarQuest(PedirInt("\nInforme o id:", (int) idMax), PedirQuest("novo(a) "));
        }
        if (resp3 == 3){
            dao.DeletarQuest(PedirInt("\nInforme o id:", (int) idMax));
        }
        
        getMenu();
    }
    
    public void Historicos() throws ClassNotFoundException, SQLException, IOException{
        
        int visualizacao1 = PedirInt("\nInforme 1 para exibir todo o histórico e 2 para eleger os Tops 5 melhore e piores", 2),
                visualizacao2 = PedirInt("\n1 - Por data\n2 - Por área e dificuldade\n3 - Somente por área\n4 - Somente por "
                + "dificuldade\n5 - Tudo\nInforme o tipo de visualização:", 5), resp = 0;
        String comando = " where Usuario_FK = " + idUsu;
        long idMax = 0;

        switch (visualizacao2){
            
            case 1:
                idMax = ExibirData(PedirString("\nInforme uma data base no formato: DD/MM/AAAA ou DD-MM-AAAA:"), visualizacao1);             
                break;

            case 2:
                resp = PedirInt("\n1 - Exatas\n2 - Humanas\n3 - Biológicas\nInforme a área:", 3);
                int resp2 = PedirInt("\n1 - Fácil\n2 - Médio\n3 - Difícil\nInforme a dificuldade:", 3);
                comando += " and Area = " + resp + " and Dificuldade = " + resp2;
                break;
                
            case 3:
                resp = PedirInt("\n1 - Exatas\n2 - Humanas\n3 - Biológicas\nInforme a área:", 3);
                comando += " and Area = " + resp;
                break;
                
            case 4:
                resp = PedirInt("\n1 - Fácil\n2 - Médio\n3 - Difíci\nInforme a dificuldade:", 3);
                comando += " and Dificuldade = " + resp;
                break;
        }
        
        if (visualizacao2 != 1){
            if (visualizacao1 == 2){
                comando += " order by MediaQuiz asc limit 5;";
                idMax = ExibirTop5(comando, "");
            } else {
                idMax = ImprimirHists(comando, "");
            }
        }
        
        DAOHistorico dao = new DAOHistorico();
        
        while (PedirInt("\nDeseja excluir um histórico? 1 - Sim e 2 - Não:", 2) == 1){
           dao.DeletarHist( (long) PedirInt("\nInforme o id:", (int) idMax), idUsu);
        }
        getMenu();
    }
    
    public long ExibirTop5 (String comando, String mensagem) throws ClassNotFoundException, SQLException{

        System.out.println(mensagem);
        
        long id1 = ImprimirHists(comando, "\nTop 5: As melhores médias:");
        long id2 = ImprimirHists(comando.replaceAll("asc", "desc"), "\nTop 5: As piores médias:");
        
        return verificaMaior(id1, id2, 0);
    }
    
    public long ExibirData (String data, int visualizacao) throws ClassNotFoundException, SQLException{
        
        String dia = data.substring(0, 2), mes = data.substring(3, 5), ano = data.substring(6, 10);
        
        String comandoDia = " where DataHora like '" + dia + "/__/%'", 
                comandoMes = " where DataHora like '__/" + mes + "/%'", 
                comandoAno = " where DataHora like '__/__/" + ano + "%'";
        long diaMax = 0, mesMax = 0, anoMax = 0;
        
        if (visualizacao == 1){
            diaMax = ImprimirHists(comandoDia, "\nPor dia:");
            mesMax = ImprimirHists(comandoMes, "\nPor mês:");
            anoMax = ImprimirHists(comandoAno, "\nPor ano:");
        } else {
            diaMax = ExibirTop5(comandoDia + " order by MediaQuiz asc limit 5", "\nPor dia:");
            mesMax = ExibirTop5(comandoMes + " order by MediaQuiz asc limit 5", "\nPor mês:");
            anoMax = ExibirTop5(comandoAno + " order by MediaQuiz asc limit 5", "\nPor ano:");
        }
        
        return verificaMaior(diaMax, mesMax, anoMax);
    }
    
    public long verificaMaior(long num1, long num2, long num3){
        if (num1 >= num2 && num1 >= num3){;
            return num1;
        } else {
            if (num2 >= num1 && num2 >= num3){
                return num2;
            } else {
                return num3;
            }
        }
    }
}